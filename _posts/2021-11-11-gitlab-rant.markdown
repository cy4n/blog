---
layout: post
title:  "gitlab features vs fixes"
date:   2021-11-11 15:32:14 +0100
categories: gitlab
---

this morning i found a tweet showing a fun gif:

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">.<a href="https://twitter.com/gitlab?ref_src=twsrc%5Etfw">@gitlab</a> when it hears there is a space in the DevOps lifecycle it doesn&#39;t currently occupy <a href="https://t.co/3FgVQrjGOk">pic.twitter.com/3FgVQrjGOk</a></p>&mdash; Mandatory Seasoning (@monkchips) <a href="https://twitter.com/monkchips/status/1458475179538067460?ref_src=twsrc%5Etfw">November 10, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 


and it kind of triggered me to write a couple of thoughts on Gitlab and my view on the their current state 

----
TLDR: please give bugs some prio compared to new features

----

The GIF in the tweet should probably mean gitlab pushing to implement new features. however i see new features that just run over old bugs and push them out of the way (back to the bench/backlog).

i love gitlab. 

I have 
* been using gitlab for a couple of years, starting somewhere or 7.x.
* installed, configured and maintained gitlab instances, i'm currently using a self-hosted one at work and gitlab.com for some side projects, blog and demo code. 
* been a heavy gitlab fan for years, both admiring their technology, but also their openness, transparency and their attitude towards diversity.
* learned a lot from the handbook, which is basically an open source manual of running a remote company, carrying most of the values i would consider THE most important at work today.
* used gitlab for demo code and live-demos on multiple conference talks because gitlab-ci is just easier to understand than anything else
* earned a mention in gitlab release notes
* gotten to know some people working at gitlab that i value and admire more than most people "on the internet"
* actually thought about applying there on multiple occasions, going so far as to actually having 1on1s with 3 people i know over there about their current job, to find out more than the job posting could ever tell. 


however there is 1 major issue that i can't wrap my head around: 
WHY the f can you not fix some bugs that have been reported by multiple people, with tickets linked from zendesk from paying customers that talk about production issues, that have been living in the issues for >5 years, with people digging it up every other month, that even have been halfway fixed only to be postponed again.

so:

**Dear Gitlab**: 
i would love for you to consider taking a step back on new stuff and fix some more issues first, please. (and yes, that has probably become harder since the IPO, because new shit triggers new customers...)

and judging from my google skills: i'm not the only grumpy old guy, i've found a lot social media and SO posts from people that feel the same, on other issues totally unrelated to the few that i really care about.

my favorite: 
anything related to [this epic around the issue of (surprise-)expanding variables](https://gitlab.com/groups/gitlab-org/-/epics/1994)
(and yes. we have workarounds in place, and it's a shitshow on every new project and developer to work around it.)
